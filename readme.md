Controller for DeLonghi EC155 espresso machine
==============================================
This is a controller I built for my DeLonghi EC155 espresso machine. It uses a PID
algorithm to control the boiler and a knob to control the pump duty cycle. There is a knob
to set the brew temperature and a switch to switch between brew and steam temperatures
(the steam temperature is hard-coded because I did not think that would need to be
adjusted much). The display shows the set temperature, current temperature, boiler duty
cycle, pump duty cycle, and a timer. The timer resets and starts counting when the pump
starts and stops when the pump shuts off.

Microcontroller
---------------
The microcontroller I'm using is an Atmega328p. I built a custom board for it that is
similar to an Arduino Uno but runs at 3.3v and 12 MHz. The screen and thermocouple board
require 3.3v, and because of the lower voltage I dropped the clock speed to 12 MHz. The
[MiniCore](https://github.com/MCUdude/MiniCore) Arduino core makes it easy to set voltage
and clock speed from the Arduino IDE.

PID Algorithm and Tuning
------------------------
Since the boiler on my machine is fairly small, I set the proportional gain to be quite a
bit higher when the pump is on to counteract the incoming cooler water.

So, if after reading this you're asking yourself, "Why would I build this instead of
buying a better espresso machine?", you should probably just buy a better espresso
machine. There's not a good reason to build this unless you really like building your own
stuff, or you would rather spend time than money.
