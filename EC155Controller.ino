#include <Wire.h>
#include <SerLCD.h>
#include <SparkFunMAX31855k.h>

const uint8_t tc_pin = 8;
const uint8_t heater = 9;
const uint8_t pump = 10;
const uint8_t temp_pot = A3;
const uint8_t pump_pot = A2;
const uint8_t temp_sw = 2;

// 833 ms is 100 half cycles at 60 Hz results in resolution of 1 in 100
// loop time turned out to be around 500 ms which is a resolution of 1 in 60 @ 60 Hz
// so trying that
const uint16_t sample_time = 500;

// 12 MHz / (1024 (prescaler) * 2 (timer counts up and down) * (pwm frequency))
const int16_t top_count = F_CPU / (1024 * 2 * (1000.0 / sample_time));
const float t_alpha = 0.66;
const float d_alpha = 0.3;
const uint8_t p_gain_idle = 4;
const uint8_t p_gain_pump = 20;
const uint8_t i_band = 10;

// time constant stores samples not seconds: seconds / (sample_time / 1000.0);
const uint8_t i_time = 106 / (sample_time / 1000.0);
const uint8_t d_time = 2.5 / (sample_time / 1000.0);

uint8_t set_temp[] = {93, 115};
uint8_t temp_index;
float raw_temp;
float temp;
float last_temp;
float d_temp;
float error;
float i_term;
int16_t heater_pwm; // may have to hold large or negative numbers but shouldn't roll over
uint8_t pump_pwm;
uint32_t total_time;
uint32_t last_time;
uint32_t start_time;
uint8_t pump_time;
uint8_t p_gain = p_gain_idle;

SparkFunMAX31855k probe(tc_pin); 
SerLCD lcd;

void setup() {
    pinMode(heater, OUTPUT);
    pinMode(pump, OUTPUT);
    pinMode(temp_pot, INPUT);
    pinMode(pump_pot, INPUT);
    pinMode(temp_sw, INPUT);

    // set up Timer 1
    TCCR1A = _BV(COM1A1) | _BV(COM1B1) | _BV(WGM11);
    TCCR1B = _BV(WGM13) | _BV(CS12) | _BV(CS10);
    ICR1 = top_count;
    OCR1A = 0;
    OCR1B = 0;

    Wire.begin();
    lcd.begin(Wire);
    lcd.setBacklight(0, 255, 0);
    lcd.setContrast(0);
    lcd.setCursor(0, 0);
    lcd.print("Initializing...");

    do {
        raw_temp = probe.readTempC();
    } while (isnan(raw_temp));
    temp = raw_temp;
    last_temp = temp;
}
void loop() {
//    uint32_t last_loop_time = total_time;
    total_time = millis();
    if (pump_pwm) {
        pump_time = (total_time - start_time) / 1000;
        p_gain = p_gain_pump;
    } else {
        start_time = total_time;
        p_gain = p_gain_idle;
    }
    if (digitalRead(temp_sw) == HIGH) {
        temp_index = 1;
    } else {
        temp_index = 0;
    }
    set_temp[0] = map(analogRead(temp_pot), 0, 1023, 90, 99);
    if (90 == set_temp[0]) {
        set_temp[0] = 0;
    }
    pump_pwm = map(analogRead(pump_pot), 0, 1023, 0, 100);

    raw_temp = probe.readTempC();
    if (!isnan(raw_temp)) {
        temp = t_alpha * raw_temp + (1 - t_alpha) * temp;
    }

    if (total_time - last_time >= sample_time) {
        d_temp = d_alpha * (temp - last_temp) + (1 - d_alpha) * d_temp;

        error = set_temp[temp_index] - temp;
        if (error > -i_band && error < i_band) {
            i_term += 1.0 / i_time * error;
        }
        i_term = (i_term > 3) ? 3 : i_term;

        heater_pwm = p_gain * (error + i_term - d_time * d_temp);

        if (heater_pwm > 100) {
            heater_pwm = 100;
        } else if (heater_pwm < 0) {
            heater_pwm = 0;
        }

        OCR1A = top_count * 0.01 * heater_pwm;
        OCR1B = top_count * 0.01 * pump_pwm;

        last_temp = temp;
        last_time = total_time;
    }

    lcd.setCursor(0, 0);
    if (set_temp[temp_index] < 100) lcd.print(' ');
    lcd.print(set_temp[temp_index]);
    lcd.print("      ");
    lcd.setCursor(9, 0);
    if (heater_pwm < 100) lcd.print(' ');
    if (heater_pwm < 10) lcd.print(' ');
    lcd.print(heater_pwm);
    lcd.print(" ");
    lcd.setCursor(13, 0);
    if (pump_pwm < 100) lcd.print(' ');
    if (pump_pwm < 10) lcd.print(' ');
    lcd.print(pump_pwm);
    lcd.setCursor(0, 1);
    if (temp < 100) lcd.print(' ');
    lcd.print(temp);
    lcd.print("       ");
//    lcd.setCursor(7, 1);
//    lcd.print(total_time - last_loop_time);
//    lcd.print(' ');
//    lcd.setCursor(12, 1);
//    lcd.print(d_time * d_temp);
    lcd.setCursor(13, 1);
    if (pump_time < 100) lcd.print(' ');
    if (pump_time < 10) lcd.print(' ');
    lcd.print(pump_time);
}
